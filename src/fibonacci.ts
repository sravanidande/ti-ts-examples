export function fibonacci (n: number): number[] {
 
  let x = 1;
   let y = 1;
   const fib = [];
   fib.push(x);
   fib.push(y);
   for (let i = 2; i < n; i++) {
     fib[i] = x + y;
     x = y;
     y = fib[i];
   }
   return fib;
    }


