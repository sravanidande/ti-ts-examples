import factorial from './factorial';

test('factorial', () => {
  expect(factorial(0)).toEqual(1)
  expect(factorial(5)).toEqual(120)
});